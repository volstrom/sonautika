#+TITLE: Sonautika Manual

#+EXPORT_FILE_NAME: tindex
# #+OPTIONS: toc:2
#+OPTIONS: toc:nil

#+HTML_HEAD: <style type="text/css">body{ max-width:800px; margin: auto } img { max-width: 100%; height: auto; }</style>

-v00052-

#+TOC: headlines 2

* Introduction
Work in progress. But some hidden features are explained.

** Features
Advanced Fretboard Explorer for Windows, MacOS and Android.
Visualize chords and scales for guitar, bass, banjo, ukulele and many more instruments.
Learn and see, which scales can be played over a chord. Which chords fit to a mode.

– Multiple diagrams at the same time.

– Fingerings for chords (Drop2, Drop3, …) and scales.

– Custom instruments, any number of strings and any tuning.

– Custom chords, scales and fingerings.

– Comping editor. Compose diagram fingerings.

* Main Concepts

** Headbar
#+ATTR_HTML: :style border:1px solid black
[[file:images/headbar_colorized.png]]
*** Main Menu (Green)
Selects the view. There are three different views.
- List-View
- Score-View
- Manager-View.
- Save
- Load.
- New: resets Sonautika to the initial state. Clears Score- and List-View.
- Settings.
*** View Interaction (Violet)
Common functions for that view.
*** View Menu (Blue)
A menu specific to the current view.
Provides e.g. transpose, print or clear view.
* Edit Diagrams
The Diagram-Editor is commonly used in Sonautika. To edit the diagrams in List- and Score-View. Also to edit and create chords, scales and patterns in Manager-View.
The Diagram-Editor consists of three sections.
** Mode Selector
Lets you choose the root, chord and scale of the diagram.
#+ATTR_HTML: :style border:1px solid black
[[file:images/diagram_editor_mode_selector.png]]
** Pattern Selector
Lets you choose a chord and scale pattern.
If the chord consists of more notes than the pattern handles, you can choose an interval to exclude.
#+ATTR_HTML: :style border:1px solid black;
[[file:images/diagram_editor_pattern_selector.png]]

To choose a base string for a chord pattern right-click (longpress on android) Chord-Pattern spinner, then select string.

#+ATTR_HTML: :style border:1px solid black;
[[file:images/gif/diagram_editor_pattern_base.gif]]

** Diagram View
Shows the whole fretboard and lets you add custom points.
#+ATTR_HTML: :style border:1px solid black;
[[file:images/diagram_editor_diagram_view.png]]


* List-View
Show multiple fretboards at once.
** View Interaction
- Metronome bpm
- Metronome toggle
- add Diagram
** Diagram edit
#+ATTR_HTML: :style border:1px solid black
[[file:images/list_view_edit.png]]
*** Edit Diagram
Opens Diagram-Editor
*** Move Diagram
Move diagram up and down in List-View
*** Delete Diagram
Remove Diagram.
** View Menu
*** Transpose
*** Export
*** Clear
Clears diagram view. Delets all diagrams. Creates one empty diagram.
* Score-View
Show, play and edit a simple score.
** View Interaction
- Bar position
- Play/pause score
- Stop score
- Metronome bpm
- Toggle metronome
- Edit score
** View Menu
*** Transpose
*** Export
*** Clear
*** Mute
Mutes playback. Scrolls score automatically without sound.
** Score-Editor
If window height high enough, editor shows below Score-View. Else whole window is editor.
To add or delete notes and diagrams either double-click or single-click in Draw-Mode.
To choose a pitch for a note: select note then click on string/fret in Diagram-Editor.
To choose a duration for a note: select note then choose a duration on Grid-Selector.
*** Grid-Selector
*** Draw-Mode
*** Chord-Input
Clicking on string/fret adds note to score on time position of selected note.

#+ATTR_HTML: :style border:1px solid black;
[[file:images/gif/score_editor_chord_input.gif]]
*** Step-Input
Clicking on string/fret sets selected note to pitch and selects next note, if no next note exists a new one is created.
Or Clicking on Grid-Selector sets selected note to duration and selects next note, if no next note exists a new one is created.

#+ATTR_HTML: :style border:1px solid black;
[[file:images/gif/score_editor_step_input.gif]]
*** Multi-select
Click and drag to select mutiple objects (notes/diagrams) with a rectangle or click on an object to (de)select object.

#+ATTR_HTML: :style border:1px solid black;
[[file:images/gif/score_editor_multi_select.gif]]
* Manager-View
Manage chords, scales, patterns and instruments.
* Settings
#+ATTR_HTML: :style border:1px solid black
[[file:images/settings.png]]

- Accidentals (Sharp/Flat): sets the sign for pitches.
- Instrument: change the instrument used for all diagrams
- Diagram Finger: sets the label on the fingers in diagrams. None, note name or interval name.
- List Dia. Size: sets the size of diagrams in List-View.
- Score Dia. Size: sets the size of diagrams in Score-View.
- Score Note Size: sets the size of notes in Score-View.
- Editor Dia. Size: sets the size of diagrams in Score-Editor-View.
- Editor Note Size: sets the size of notes in Score-Editor-View.
- Editor Dia. Play Sample: if on, plays a note if a fret is pressed in a diagram.
- Data Path: sets the directory of Sonautika. If new directory is empty, actual directory gets copied. If new directory is not empty, directory will be used. That means you could use different directories, with differnt chords, scales, instruments, songs...
- Animation: toggles animations.
* Export
#+ATTR_HTML: :style border:1px solid black
[[file:images/export_score_view.png]]

Exports a PNG image.
The first row lets you set a name. It is also the title, displayed in the exported file.
The second row lets you set the file to be exported.
The DPI settings sets the resolution of the experted file.
Only Score export. The size sets the size of the diagrams and notes.
* Advanced Features
** Sync Devices
** Adapt GUI Size
** Directory Structure
